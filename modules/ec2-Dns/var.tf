variable "ami_name" {
  default = "ami-08f3712c8ca5af75e"
}

variable "instance_type" {
  default = "t2.micro"
}
