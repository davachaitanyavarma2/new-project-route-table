resource "aws_route_table" "public-test" {
  vpc_id = "${var.vpc_id}"

  
  tags = {
    Name = "public-test1"
  }
}

resource "aws_route_table" "private-test" {
  vpc_id = "${var.vpc_id}"

  tags = {
      Name = "Private-test1"
  }

}
