provider "aws" {
 # profile    = "default"
  region     = "${var.aws_region}"
}

#resource "aws_instance" "example" {
 # ami           = "${var.ami_name}"
 # instance_type = "${var.instance_type}"
#}

module "subnet-rts" {

  source = "./modules/vpc/Route table"
}

module "ec2-instance" {

  source = "./modules/ec2-Dns"
}

